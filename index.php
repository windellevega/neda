<!DOCTYPE html>
<html>
<head>
	<title>NEDA-R02</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="shortcut icon" href="img/favicon.png">
</head>
<body>
	<div class="container" id="header">
		<div id="header-logo" class="row" >
			<center>
				<h1><img src="img/NEDA-logo.png" style="max-height:60px" /> NEDA</h1><h4>National Economic and Development Authority Region-02</h4>
			</center>
		</div>
		<div id="navigation" class="row">
			<ul class="nav nav-pills">
				<li><a href="index.php"><h5><span class="glyphicon glyphicon-home"></span> Home</h5></a></li>
				<li><a href="#"><h5><span class="glyphicon glyphicon-info-sign"></span> About Us</h5></a></li>
				<li><a href="#"><h5><span class="glyphicon glyphicon-flash"></span> Developers</h5></a></li>
			</ul>
		</div>
	</div>
	<!--<div class="container" id="body">-->
		<div id="img-slider" class="row">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
					<li data-target="#carousel-example-generic" data-slide-to="4"></li>
				</ol>
				<div class="carousel-inner">
					<div class="item active">
						<img src="img/1.jpg" alt="...">
						<div class="carousel-caption">
							<h4>This is me! Windelle John G. Vega!</h4>
						</div>
					</div>
					<div class="item">
					  <img src="img/2.jpg" alt="...">
					  <div class="carousel-caption">
					   <h4>wewewe</h4>
					  </div>
					</div>
					<div class="item">
					  <img src="img/3.jpg" alt="...">
					  <div class="carousel-caption">
					   <h4>wewewe</h4>
					  </div>
					</div>
					<div class="item">
					  <img src="img/4.jpg" alt="...">
					  <div class="carousel-caption">
					   <h43>wewewe</h4>
					  </div>
					</div>
					<div class="item">
					  <img src="img/5.jpg" alt="...">
					  <div class="carousel-caption">
					   <h43>wewewe</h4>
					  </div>
					</div>

				</div>
				<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>
		<br/>
		<div id="system-thumbnails" class="row">
			<div class="col-md-3" id="ATS">
				<center>
					<a href="neda-ats">
						<img class="img-circle" src="img/ats.png" style="border: 10px solid #543d15" />	
					</a>
					<h4>Assignment Tracking System</h4>
				</center>
			</div>
			<div class="col-md-3" id="SIS">
				<center>
					<a href="http://192.168.222.100/codec/sis/loginform.php">
						<img class="img-circle" src="img/sis.png" style="border: 10px solid #2e7427"/>
					</a>
					<h4>Supplies Information System</h4>
				</center>
			</div>
			<div class="col-md-3" id="PPEIS">
				<center>
					<a href="#">
						<img class="img-circle" src="img/ppeis.png" style="border: 10px solid #949f1b"/>
					</a>
					<h4>Property Plant and Equipment Inventory System</h4>
				</center>
			</div>
			<div class="col-md-3" id="PIS">
				<center>
					<a href="#">
						<img class="img-circle" src="img/pis.png" style="border: 10px solid #912121"/>
					</a>
					<h4>Personnel Information System</h4>
				</center>
			</div>
		</div>
	<!--</div>-->
	<script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
<footer>
	<hr/>
	<center>
		<p>Copyright &copy; 2013 | All Rights Reseved NEDA-R02</p>
		<p>www.neda.gov.ph</p>
	</center>	
</footer>
</html>