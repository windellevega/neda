<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sample SQL Connection</title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="../img/favicon.png">
</head>
<header>
	<div class="navbar navbar-inverse">
		<center>
			<h1 style="color:#48abff">NEDA-ATS</h1>
			<h4 style="color:#48abff">Assignment Tracking System</h4>
		</center>
	</div>
	<div id="header" class="container">
		<div class="col-md-4 col-md-offset-4">
			<center>
			<ul class="nav nav-pills">
				<li><a href="http://localhost/neda"><h5><span class="glyphicon glyphicon-home"></span> Home</h5></a></li>
				<li><a href="select.php"><h5><span class="glyphicon glyphicon-list"></span> Select</h5></a></li>
				<li><a href="insert.php"><h5><span class="glyphicon glyphicon-plus"></span> Add</h5></a></li>
				
				<li>
				  <a data-toggle="dropdown" href="#"><h5><span class="glyphicon glyphicon-edit"></span> Modify<span class="caret"></span></h5></a>
				  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
				  	<li><a href="edit.php"><h5><span class="glyphicon glyphicon-pencil"></span> Edit</h5></a></li>
					<li><a href="delete.php"><h5><span class="glyphicon glyphicon-remove"></span> Delete</h5></a></li>
				  </ul>
				</li>
			</ul>
			</center>
		</div>
	</div>
</header>
<body>
	<div id="body" class="container">