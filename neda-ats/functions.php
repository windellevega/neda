<?php
function clean($str,$encode_ent = false)
{
	$str = @trim($str);

	if($encode_ent)
	{
		$str = htmlentities($str);
	}
	if(version_compare(phpversion(), '4.3.0') >= 0)
	{
		if(get_magic_quotes_gpc())
		{
			$str = stripcslashes($str);
		}
		if(@mysql_ping())
		{
			$str = mysql_real_escape_string($str);
		}
		else
		{
			$str = addslashes($str);
		}
	}
	else
	{
		if(!get_magic_quotes_gpc())
		{
			$str = addslashes($str);
		}
	}
	return $str;
}
?>