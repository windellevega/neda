<?php
	include("template_header.php");
  ?>
		<br/>
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-primary" style="box-shadow: 1px 3px 8px black;">
				<div class="panel-heading">
					<strong>REGISTRATION</strong>
				</div>
				<div class="panel-body">
					<form method="post" action="insertaction.php">
						<strong>First Name</strong>
						<input type="text" placeholder="First Name" name="firstname" class="form-control" /><br/>
						<strong>Middle Name</strong>
						<input type="text" placeholder="Middle Name" name="middlename" class="form-control" /><br/>
						<strong>Last Name</strong>
						<input type="text" placeholder="Last Name" name="lastname" class="form-control" class="form-control" /><br/>
						<strong>Gender</strong>
						<select placeholder="Gender" name="gender" class="form-control"><br/>
							<option>Male</option>
							<option>Female</option>
						</select><br/>
						<strong>Address</strong>
						<input type="text" placeholder="Address" name="address" class="form-control" /><br/>
						<center>
							<input type="submit" value="Register" class="btn btn-primary">
						</center>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php
	include("template_footer.php");
  ?>