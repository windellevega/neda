<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sample SQL Connection</title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="../img/favicon.png">
</head>
<header>
	<div class="navbar navbar-inverse">
		<center>
			<h1 style="color:#48abff">NEDA-ATS</h1>
			<h4 style="color:#48abff">Assignment Tracking System</h4>
		</center>
	</div>
</header>
<body>
	<div id="body" class="container">
		<br/>
		<div class="row">
			<div class="col-md-4 col-md-offset-4 well" style="border: 1px solid #7c7c7c">	
					<center>
							<form method="post" action="select.php">
								<strong>Username</strong>
								<input type="text" placeholder="Username" name="username" class="form-control" /><br/>
								<strong>Password</strong>
								<input type="password" placeholder="Password" name="password" class="form-control" /><br/>
								<button type="submit" class="btn btn-primary">Login</button>
							</form>
						</center>	
				<!--<div class="panel panel-primary" style="box-shadow: 1px 3px 8px black;">
					<div class="panel-heading">
						<strong>LOGIN</strong>
					</div>
					<div class="panel-body">
						<center>
							<form method="post" action="select.php">
								<strong>Username</strong>
								<input type="text" placeholder="Username" name="username" class="form-control" /><br/>
								<strong>Password</strong>
								<input type="password" placeholder="Password" name="password" class="form-control" /><br/>
								<button type="submit" class="btn btn-primary">Login</button>
							</form>
						</center>
					</div>
				</div>-->
			</div>
		</div>
<?php
	include("template_footer.php");
  ?>