<?php
	include("template_header.php");
?>
<div class="well" style="border: 1px solid #7c7c7c">
	<table class="table table-striped table-hover">
		<thead>
			<th>First Name</th>
			<th>Middle Name</th>
			<th>Last Name</th>
			<th>Gender</th>
			<th>Address</th>
			<th>Action</th>
		</thead>
		<?php
			include("connection.php");
			$query = mysql_query("SELECT * FROM employee") or die(msql_error());
			while($row = mysql_fetch_array($query))
			{
		?>
		<tr>
			<td><?php echo $row[0];?></td>
			<td><?php echo $row[1];?></td>
			<td><?php echo $row[2];?></td>
			<td><?php echo $row[3];?></td>
			<td><?php echo $row[4];?></td>
			<td><?php echo $row[5];?></td>
			<td>
				<a href="#" data-toggle="modal" data-target="#myModal<?php echo $row[0]?>"><i class="glyphicon glyphicon-remove"></i>Delete</a>
				<div class="modal fade" id="myModal<?php echo $row[0]?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				        <h4 class="modal-title" id="myModalLabel">Confirm Action</h4>
				      </div>
				      <div class="modal-body">
				        Do you really want to delete record?
				      </div>
				      <div class="modal-footer">
				      	<a type="button" class="btn btn-primary" href="deleteaction.php?id=<?php echo $row[0];?>">Delete</a>
				        <a type="button" class="btn btn-default" data-dismiss="modal">Cancel</a>
				      </div>
				    </div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				</div>
			</td>	
		</tr>
		<?php
			}
		?>
	</table>
</div>
<?php
	include("template_footer.php");
  ?>